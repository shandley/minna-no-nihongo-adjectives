# minna-no-nihongo-adjectives

A collection of adjectives which appear in the Minna no Nihongo (みんなの日本語) second edition textbook, provided in JSON format.

This is provided as a resource to students creating study tools while using the textbook.

## installation

`npm install minna-no-nihongo-adjectives`

## Usage

```javascript
import adjectives from 'minna-no-nihongo-adjectives'

const chapterTwelveAdjectives = adjectives.filter(a => a.chapter === 12)

/**
 *
 [
   {
    "id": "wFzvtufLk",
    "adjective_type": 0,
    "hiragana": "かんたん",
    "english": "easy; simple",
    "kanji": "簡単",
    "chapter": 12,
    "past": "かんたかった",
    "negative": "かんたくない",
    "past_negative": "かんたくなかった"
  },
  ...
 ]
 */
```
Each adjective item is provided in following format:

```typescript
const enum AdjectiveType {
  I, // 0
  NA // 1
}

export interface Adjective {
  id: string
  adjective_type: AdjectiveType // See above, either 0 (い) or 1 (な)
  hiragana: string
  english: string
  kanji: string
  chapter: number,
  past: string,
  negative: string
  past_negative: string
}
```
Id's are generated using [shortId](https://github.com/dylang/shortid). In cases where kanji does not exist or could not be determined (eg. いい) the hirigana is substituted.

The past tense for な adjectives is the plain hiragana value.

## Issues

Please report any missing or incorrect entries via [gitlab](https://gitlab.com/shandley/minna-no-nihongo-adjectives/issues).

## Changelog

```
0.1.0 Add tenses and negatives for each item

0.0.5 Move to gitlab, update documentation, insert missing kanji

0.0.4 Corrections

0.0.3 Added missing items from later chapters

0.0.2 Documentation

0.0.1 Initial publication
```