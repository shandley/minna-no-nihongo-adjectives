declare const enum AdjectiveType {
    I = 0,
    NA = 1
}
export interface Adjective {
    id: string;
    adjective_type: AdjectiveType;
    hiragana: string;
    english: string;
    kanji: string;
    chapter: number;
    past: string;
    negative: string;
    past_negative: string;
}
declare const adjectives: Adjective[];
export default adjectives;
